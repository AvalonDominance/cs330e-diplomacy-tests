#!/usr/bin/env python3

# -------
# imports
# -------

from io import StringIO
from unittest import main, TestCase

from Diplomacy import diplomacy_solve

# -----------
# TestDiplomacy
# -----------

class TestDiplomacy(TestCase):

    # -----------
    # solve
    # -----------
    def test_diplomacy_solve_1(self):
        r = StringIO('A Seattle Hold\nB Cluj Move Seattle\nC Paris Support B\nD Dallas Move Paris')
        w = StringIO()
        diplomacy_solve(r,w)
        self.assertEqual(w.getvalue(), 'A [dead]\nB [dead]\nC [dead]\nD [dead]\n')

    def test_diplomacy_solve_2(self):
        r = StringIO('A Barcelona Move Dubai\nB Dubai Move Barcelona\nC Paris Support A\n')
        w = StringIO()
        diplomacy_solve(r,w)
        self.assertEqual(w.getvalue(), 'A Dubai\nB Barcelona\nC Paris\n')

    def test_diplomacy_solve_3(self):
        r = StringIO('')
        w = StringIO()
        diplomacy_solve(r,w)
        self.assertEqual(w.getvalue(), '')

    def test_diplomacy_solve_4(self):
        r = StringIO('A Barcelona Move Dubai\nB Dubai Hold\n')
        w = StringIO()
        diplomacy_solve(r,w)
        self.assertEqual(w.getvalue(), 'A [dead]\nB [dead]\n')

    def test_diplomacy_solve_5(self):
        r = StringIO('A Barcelona Hold\nB Dubai Move Barcelona\nC Paris Support A\n')
        w = StringIO()
        diplomacy_solve(r,w)
        self.assertEqual(w.getvalue(), 'A Barcelona\nB [dead]\nC Paris\n')

    def test_diplomacy_solve_6(self):
        r = StringIO('A Barcelona Hold\nB Prague Move Barcelona\nC Venice Move Barcelona\nD Cairo Support C\n')
        w = StringIO()
        diplomacy_solve(r,w)
        self.assertEqual(w.getvalue(), 'A [dead]\nB [dead]\nC Barcelona\nD Cairo\n')

    def test_diplomacy_6(self):
        r = StringIO('A Austin Support B\nB Dallas Hold\nC ElPaso Move Dallas\n')
        w = StringIO()
        diplomacy_solve(r,w)
        self.assertEqual(w.getvalue(), 'A Austin\nB Dallas\nC [dead]\n')


# ----
# main
# ----

if __name__ == "__main__":
    main()

""" # pragma: no cover

.......
----------------------------------------------------------------------
Ran 7 tests in 0.001s

OK
Name               Stmts   Miss Branch BrPart  Cover   Missing
--------------------------------------------------------------
Diplomacy.py          90      0     50      0   100%
TestDiplomacy.py      39      0      0      0   100%
--------------------------------------------------------------
TOTAL                129      0     50      0   100%

"""

    
        
