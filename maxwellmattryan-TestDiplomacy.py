from io import StringIO
from unittest import main, TestCase

from Diplomacy import (
    Army, 
    diplomacy_read_input, 
    diplomacy_init_army, 
    diplomacy_update_support_count, 
    diplomacy_create_initial_cities, 
    diplomacy_update_cities, 
    diplomacy_eval, 
    diplomacy_print, 
    diplomacy_solve
)

class DiplomacyTest(TestCase):
    # ARMY CLASS
    def test_army_eq_01(self):
        a = Army("A", "Madrid", "Hold")
        b = Army("B", "Barcelona", "Move", "Madrid")
        self.assertNotEqual(a, b)

    def test_army_eq_02(self):
        a = Army("A", "Madrid", "Hold")
        b = Army("A", "Madrid", "Hold")
        self.assertEqual(a, b)

    def test_army_eq_03(self):
        a = Army("A", "Madrid", "Hold")
        self.assertNotEqual(a, "army")

    def test_army_str_01(self):
        a = Army("A", "Madrid", "Hold")
        string = "A    Madrid       Hold                      0"
        self.assertEqual(str(a), string) 

    def test_army_str_02(self):
        a = Army("A", "Madrid", "Move", "Austin")
        string = "A    Madrid       Move         Austin       0"
        self.assertEqual(str(a), string) 

    # INPUT READING
    def test_read_input_01(self):
        input_buffer = StringIO("A Madrid Hold")
        result = diplomacy_read_input(input_buffer)

        actual = { "A": Army("A", "Madrid", "Hold") }

        self.assertDictEqual(result, actual)

    def test_read_input_02(self):
        input_buffer = StringIO("A Madrid Hold\nB Barcelona Move Madrid\nC London Support B")
        result = diplomacy_read_input(input_buffer)

        actual = {
            "A": Army("A", "Madrid", "Hold"),
            "B": Army("B", "Barcelona", "Move", "Madrid"),
            "C": Army("C", "London", "Support", "B")
        }

        self.assertDictEqual(result, actual)

    def test_read_input_03(self):
        input_buffer = StringIO("A Madrid Hold\nB Barcelona Move Madrid\nC London Move Madrid\nD Paris Support B\nE Austin Support A")
        result = diplomacy_read_input(input_buffer)

        actual = {
            "A": Army("A", "Madrid", "Hold"),
            "B": Army("B", "Barcelona", "Move", "Madrid"),
            "C": Army("C", "London", "Move", "Madrid"),
            "D": Army("D", "Paris", "Support", "B"),
            "E": Army("E", "Austin", "Support", "A"),
        }

        self.assertDictEqual(result, actual)

    # ARMY INITIALIZATION
    def test_init_army_01(self):
        input_string = "A Madrid Hold"
        result = diplomacy_init_army(input_string)

        actual = Army("A", "Madrid", "Hold")

        self.assertEqual(result, actual)

    def test_init_army_02(self):
        input_string = "A Madrid Move Barcelona"
        result = diplomacy_init_army(input_string)

        actual = Army("A", "Madrid", "Move", "Barcelona")

        self.assertEqual(result, actual)

    def test_init_army_03(self):
        input_string = "A Madrid Support B"
        result = diplomacy_init_army(input_string)

        actual = Army("A", "Madrid", "Support", "B")

        self.assertEqual(result, actual)


    # UPDATING SUPPORT COUNTS
    def test_update_support_count_01(self):
        armies = {
            "A": Army("A", "Madrid", "Hold"),
            "B": Army("B", "Barcelona", "Move", "Madrid")
        }
        cities = diplomacy_create_initial_cities(armies)
        result = diplomacy_update_support_count(armies, cities)

        actual = {
            "A": Army("A", "Madrid", "Hold", support_count = 0),
            "B": Army("B", "Barcelona", "Move", "Madrid", support_count = 0)
        }

        self.assertEqual(result, actual)

    def test_update_support_count_02(self):
        armies = {
            "A": Army("A", "Madrid", "Hold"),
            "B": Army("B", "Barcelona", "Move", "Madrid"),
            "C": Army("C", "London", "Support", "B")
        }
        cities = diplomacy_create_initial_cities(armies)
        result = diplomacy_update_support_count(armies, cities)

        actual = {
            "A": Army("A", "Madrid", "Hold", support_count = 0),
            "B": Army("B", "Barcelona", "Move", "Madrid", support_count = 1),
            "C": Army("C", "London", "Support", "B", support_count = 0)
        }

        self.assertEqual(result, actual)

    def test_update_support_count_03(self):
        armies = {
            "A": Army("A", "Madrid", "Hold"),
            "B": Army("B", "Barcelona", "Move", "Madrid"),
            "C": Army("C", "London", "Support", "B"),
            "D": Army("D", "Austin", "Support", "B"),
            "E": Army("E", "Houston", "Support", "C")
        }
        cities = diplomacy_create_initial_cities(armies)
        result = diplomacy_update_support_count(armies, cities)

        actual = {
            "A": Army("A", "Madrid", "Hold", support_count = 0),
            "B": Army("B", "Barcelona", "Move", "Madrid", support_count = 2),
            "C": Army("C", "London", "Support", "B", support_count = 1),
            "D": Army("D", "Austin", "Support", "B", support_count = 0),
            "E": Army("E", "Houston", "Support", "C", support_count = 0)
        }

        self.assertEqual(result, actual)

    # CITY INITIALIZATION
    def test_diplomacy_create_initial_cities_01(self):
        armies = {
            "A": Army("A", "Madrid", "Hold"),
            "B": Army("B", "Barcelona", "Move", "Madrid")
        }
        result = diplomacy_create_initial_cities(armies)

        actual = {
            "Madrid": [Army("A", "Madrid", "Hold")],
            "Barcelona": [Army("B", "Barcelona", "Move", "Madrid")]
        }

        self.assertEqual(result, actual)

    def test_diplomacy_create_initial_cities_02(self):
        armies = {
            "A": Army("A", "Madrid", "Hold"),
            "B": Army("B", "Barcelona", "Move", "Madrid"),
            "C": Army("C", "London", "Support", "B")
        }
        result = diplomacy_create_initial_cities(armies)

        actual = {
            "Madrid": [Army("A", "Madrid", "Hold")],
            "Barcelona": [Army("B", "Barcelona", "Move", "Madrid")],
            "London": [Army("C", "London", "Support", "B")]
        }

        self.assertEqual(result, actual)
    
    def test_diplomacy_create_initial_cities_03(self):
        armies = {
            "A": Army("A", "Madrid", "Hold"),
            "B": Army("B", "Barcelona", "Move", "Madrid"),
            "C": Army("C", "London", "Support", "B"),
            "D": Army("D", "Austin", "Support", "B"),
            "E": Army("E", "Houston", "Support", "C")
        }
        result = diplomacy_create_initial_cities(armies)

        actual = {
            "Madrid": [Army("A", "Madrid", "Hold")],
            "Barcelona": [Army("B", "Barcelona", "Move", "Madrid")],
            "London": [Army("C", "London", "Support", "B")],
            "Austin": [Army("D", "Austin", "Support", "B")],
            "Houston": [Army("E", "Houston", "Support", "C")]
        }

        self.assertEqual(result, actual)

    def test_diplomacy_create_initial_cities_04(self):
        armies = {
            "A": Army("A", "Madrid", "Hold"),
            "B": Army("B", "Barcelona", "Move", "Madrid"),
            "C": Army("C", "Madrid", "Hold")
        }
        result = diplomacy_create_initial_cities(armies)

        actual = {
            "Madrid": [Army("A", "Madrid", "Hold"), Army("C", "Madrid", "Hold")],
            "Barcelona": [Army("B", "Barcelona", "Move", "Madrid")]
        }

        self.assertEqual(result, actual)

    #CITY UPDATE
    def test_diplomacy_update_cities_01(self):
        armies = {
            "A": Army("A", "Madrid", "Hold"),
            "B": Army("B", "Barcelona", "Move", "Madrid"),
            "C": Army("C", "London", "Support", "B")
        }
        
        cities = diplomacy_create_initial_cities(armies)
        updated = diplomacy_update_cities(armies,cities)

        actual = {
            "Madrid": [Army("A", "Madrid", "Hold"),Army("B", "Madrid", "Move", "Madrid")],
            "Barcelona": [],
            "London": [Army("C", "London", "Support", "B")]
        }

        self.assertEqual(updated, actual)

    def test_diplomacy_update_cities_02(self):
        armies = {
            "A": Army("A", "Madrid", "Hold"),
            "B": Army("B", "Barcelona", "Move", "Madrid"),
            "C": Army("C", "London", "Support", "B"),
            "D": Army("D", "Austin", "Move", "London")
        }
        
        cities = diplomacy_create_initial_cities(armies)
        updated = diplomacy_update_cities(armies,cities)

        actual = {
            "Madrid": [Army("A", "Madrid", "Hold"),Army("B", "Madrid", "Move", "Madrid")],
            "Barcelona": [],
            "London": [Army("C", "London", "Support", "B"),Army("D", "London", "Move", "London")],
            "Austin": []
        }

        self.assertEqual(updated, actual)

    def test_diplomacy_update_cities_03(self):
        armies = {
            "A": Army("A", "Madrid", "Hold"),
            "B": Army("B", "Barcelona", "Move", "Madrid"),
            "C": Army("C", "London", "Support", "B"),
            "D": Army("D", "Austin", "Support", "B"),
            "E": Army("E", "Houston", "Move", "Austin")
        }

        cities = diplomacy_create_initial_cities(armies)
        updated = diplomacy_update_cities(armies,cities)

        actual = {
            "Madrid": [Army("A", "Madrid", "Hold"),Army("B", "Madrid", "Move", "Madrid")],
            "Barcelona": [],
            "London": [Army("C", "London", "Support", "B")],
            "Austin": [Army("D", "Austin", "Support", "B"),Army("E", "Austin", "Move", "Austin")],
            "Houston": []
        }

        self.assertEqual(updated, actual)

    def test_diplomacy_update_cities_04(self):
        armies = {
            "A": Army("A", "Madrid", "Hold"),
            "B": Army("B", "Barcelona", "Move", "Madrid"),
            "C": Army("C", "London", "Move", "Austin")
        }
        
        cities = diplomacy_create_initial_cities(armies)
        updated = diplomacy_update_cities(armies,cities)

        actual = {
            "Madrid": [Army("A", "Madrid", "Hold"),Army("B", "Madrid", "Move", "Madrid")],
            "Barcelona": [],
            "London": [],
            "Austin": [Army("C", "Austin", "Move", "Austin")]
        }

        self.assertEqual(updated, actual)

    #EVALUATION
    def test_diplomacy_eval_01(self):
        armies = {
            "A": Army("A", "Madrid", "Hold"),
            "B": Army("B", "Barcelona", "Move", "Madrid"),
            "C": Army("C", "London", "Move", "Madrid"),
            "D": Army("D", "Paris", "Support", "B"),
            "E": Army("E", "Austin", "Support", "A")
        }
        cities = diplomacy_create_initial_cities(armies)
        cities = diplomacy_update_cities(armies, cities)
        armies = diplomacy_update_support_count(armies, cities)
        result = diplomacy_eval(armies, cities)

        actual = {
            'A': 'dead',
            'B': 'dead',
            'C': 'dead',
            'D': 'Paris',
            'E': 'Austin'
        }

        self.assertEqual(result, actual)

    def test_diplomacy_eval_02(self):
        armies = {
            "A": Army("A", "Madrid", "Hold"),
            "B": Army("B", "Barcelona", "Move", "Madrid"),
            "C": Army("C", "London", "Move", "Madrid"),
            "D": Army("D", "Paris", "Support", "B")
        }
        cities = diplomacy_create_initial_cities(armies)
        cities = diplomacy_update_cities(armies, cities)
        armies = diplomacy_update_support_count(armies, cities)
        result = diplomacy_eval(armies, cities)

        actual = {
            'A': 'dead',
            'B': 'Madrid',
            'C': 'dead',
            'D': 'Paris'
        }

        self.assertEqual(result, actual)

    def test_diplomacy_eval_03(self):
        armies = {
            "A": Army("A", "Madrid", "Hold"),
            "B": Army("B", "Barcelona", "Hold"),
        }
        cities = diplomacy_create_initial_cities(armies)
        cities = diplomacy_update_cities(armies, cities)
        armies = diplomacy_update_support_count(armies, cities)
        result = diplomacy_eval(armies, cities)

        actual = {
            'A': 'Madrid',
            'B': 'Barcelona'
        }

        self.assertEqual(result, actual)

    #PRINT
    def test_print(self):
        w = StringIO()

        armies = {
            "A": Army("A", "Madrid", "Hold"),
            "B": Army("B", "Barcelona", "Move", "Madrid"),
            "C": Army("C", "London", "Move", "Madrid"),
            "D": Army("D", "Paris", "Support", "B")
        }

        c = diplomacy_create_initial_cities(armies)
        c = diplomacy_update_cities(armies, c)
        a = diplomacy_update_support_count(armies, c)
        v = diplomacy_eval(a, c)

        diplomacy_print(w, v)
        self.assertEqual(w.getvalue(), "A [dead]\nB Madrid\nC [dead]\nD Paris\n")
    
    #SOLVE
    def test_solve_01(self):
        r = StringIO("A Madrid Hold\nB Barcelona Move Madrid\n")
        w = StringIO()
        diplomacy_solve(r, w)
        self.assertEqual(
            w.getvalue(), "A [dead]\nB [dead]\n")

    def test_solve_02(self):
        r = StringIO("A Madrid Hold\nB Barcelona Move Madrid\nC London Support B\n")
        w = StringIO()
        diplomacy_solve(r, w)
        self.assertEqual(
            w.getvalue(), "A [dead]\nB Madrid\nC London\n")

    def test_solve_03(self):
        r = StringIO("A Madrid Hold\nB Barcelona Move Madrid\nC London Move Madrid\nD Paris Support B\nE Austin Support A\n")
        w = StringIO()
        diplomacy_solve(r, w)
        self.assertEqual(
            w.getvalue(), "A [dead]\nB [dead]\nC [dead]\nD Paris\nE Austin\n")

if __name__ == "__main__": #pragma: no cover
    main()
