# -------
# imports
# -------

from unittest import main, TestCase

from Diplomacy import *


# -----------
# TestCollatz
# -----------


class TestCollatz(TestCase):
    # ----
    # read
    # ----

    def test_1(self):
        book = ["A Madrid Hold", "B Barcelona Move Madrid", "C London Support B"]
        bank = {}
        for line in book:
            diplomacy_handle(bank,diplomacy_read(line))
        diplomacy_game(bank)
        ls = []
        for item in bank:
            ls.append(item + " " + bank[item]['location'])
        self.assertEqual(ls, ['A [Dead]', 'B Madrid', 'C London'])

    def test_2(self):
        book = ["A Madrid Hold", "B Madrid Move Madrid"]
        bank = {}
        for line in book:
            diplomacy_handle(bank,diplomacy_read(line))
        diplomacy_game(bank)
        ls = []
        for item in bank:
            ls.append(item + " " + bank[item]['location'])
        self.assertEqual(ls, ['A [Dead]', 'B [Dead]'])

    def test_3(self):
        book = ['A Madrid Hold', 'B Barcelona Move Madrid', 'C London Move Madrid', 'D Paris Support B',
                'E Austin Support A']
        bank = {}
        for line in book:
            diplomacy_handle(bank,diplomacy_read(line))
        diplomacy_game(bank)
        ls = []
        for item in bank:
            ls.append(item + " " + bank[item]['location'])
        self.assertEqual(ls, ['A [Dead]', 'B [Dead]', 'C [Dead]', 'D Paris', 'E Austin'])


if __name__ == "__main__":
    main()

""" #pragma: no cover
$ coverage run --branch TestCollatz.py >  TestCollatz.out 2>&1


$ cat TestCollatz.out
.......
----------------------------------------------------------------------
Ran 7 tests in 0.000s
OK


$ coverage report -m                   >> TestCollatz.out



$ cat TestCollatz.out
.......
----------------------------------------------------------------------
Ran 7 tests in 0.000s
OK
Name             Stmts   Miss Branch BrPart  Cover   Missing
------------------------------------------------------------
Collatz.py          12      0      2      0   100%
TestCollatz.py      32      0      0      0   100%
------------------------------------------------------------
TOTAL               44      0      2      0   100%
"""
